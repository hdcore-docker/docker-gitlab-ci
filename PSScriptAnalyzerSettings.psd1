@{
    Severity     = @('Information', 'Error', 'Warning')
    ExcludeRules = @('PSAvoidUsingWriteHost', 'PSAvoidUsingInvokeExpression', 'PSAvoidUsingCmdletAliases', 'PSUseApprovedVerbs')
}

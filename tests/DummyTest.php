<?php

/**
 * Class file for DummyTest
 *
 * PHP version 8.1
 *
 * @category Test
 * @package  Test
 * @author   Danny Test <danny@sandbox.com>
 * @license  Only for me
 * @version  GIT: 2023-10-15 19:10
 * @link     www.sandbox.com
 */

declare(strict_types=1);

namespace Tests;

use TestCI\Dummy;
use PHPUnit\Framework\TestCase;

/**
 * Class DummyTest
 *
 * @category Test
 * @package  Test
 * @author   Danny Test <danny@sandbox.com>
 * @license  Only for me
 * @link     www.sandbox.com
 */
final class DummyTest extends TestCase
{
    /**
     * Test getName
     */
    public function testGetName(): void
    {
        $dummy = new Dummy('dummy');
        self::assertSame('dummy', $dummy->getName());
    }
}

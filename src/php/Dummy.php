<?php

/**
 * Class file for Dummy
 *
 * PHP version 8.1
 *
 * @category Base
 * @package  Test
 * @author   Danny Test <danny@sandbox.com>
 * @license  Only for me
 * @version  GIT: 2023-10-15 19:10
 * @link     www.sandbox.com
 */

declare(strict_types=1);

namespace TestCI;

/**
 * Class for Dummy
 *
 * @category Base
 * @package  Test
 * @author   Danny Test <danny@sandbox.com>
 * @license  Only for me
 * @link     www.sandbox.com
 */
class Dummy
{
    /**
     * Name
     */
    protected string $name;

    /**
     * Constructor
     *
     * @param string $name name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     */
    public function getName(): string
    {
        return $this->name;
    }
}

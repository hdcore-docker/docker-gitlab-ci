export default {
  extends: [
    'stylelint-config-standard-scss',
  ],
  ignoreFiles: [
    'brol/**',
  ],
  rules: {
    'no-descending-specificity': null,
    'scss/at-import-no-partial-leading-underscore': null,
    'scss/dollar-variable-pattern': null,
  },
  reportInvalidScopeDisables: true,
  reportNeedlessDisables: true,
};

module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2018,
  },
  env: {
    browser: true,
    es2018: true,
  },
  extends: [
    'airbnb-base',
    'plugin:jsdoc/recommended',
  ],
  ignorePatterns: [
    'brol/',
    'tmp/',
    'www/js/',
  ],
  plugins: [
    '@typescript-eslint',
    'compat',
    'import',
    'jsdoc',
    'json-schema-validator',
    'jsonc',
    'regexp',
    'sonarjs',
    'toml',
    'yml',
  ],
  reportUnusedDisableDirectives: true,
  overrides: [
    {
      files: ['**/*.js', '**/*.cjs', '**/*.mjs'],
      extends: [
        'airbnb-base',
        'eslint:recommended',
        'plugin:compat/recommended',
        'plugin:jsdoc/recommended',
      ],
      rules: {
        'import/extensions': 0,
        'import/no-extraneous-dependencies': 0,
        'import/no-unresolved': 0,
        'jsdoc/check-values': 0,
        'jsdoc/tag-lines': 0,
        'max-len': ['error', { code: 300 }],
      },
    },
    {
      files: ['**/*.ts', '**/*.mts', '**/*.cts'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: './tsconfig.eslint.json',
        tsconfigRootDir: __dirname,
      },
      extends: [
        'airbnb-base',
        'eslint:recommended',
        'plugin:compat/recommended',
        'plugin:jsdoc/recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/stylistic-type-checked',
        'plugin:@typescript-eslint/strict-type-checked',
      ],
      rules: {
        'import/extensions': 0,
        'import/no-extraneous-dependencies': 0,
        'import/no-unresolved': 0,
        'jsdoc/check-values': 0,
        'jsdoc/tag-lines': 0,
        'max-len': ['error', { code: 300 }],
      },
    },
  ],
};
